<?php
/**
 * Home
 *
 * Standard loop for the blog-page
 */
get_header(); ?>

	<div class="main-container">
		<main>
			<div class="title-filter">
				<h1 class="main-title"><?php echo get_the_title(get_option('page_for_posts')); ?></h1>
				<button class="filter-button" hidden>
					<svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path d="M6.66667 12H9.33333V10.6667H6.66667V12ZM2 4V5.33333H14V4H2ZM4 8.66667H12V7.33333H4V8.66667Z"
									fill="white"/>
					</svg>
					<span>Фільтр</span>
				</button>
			</div>
			<?php echo do_shortcode('[ajax_load_more_filters id="bihusnews" target="bihus"]') ?>
			<?php echo do_shortcode('[ajax_load_more id="bihus" progress_bar="true" progress_bar_color="f58468" container_type="div" button_label="" target="bihusnews" filters="true" posts_per_page="9" post_type="post"]') ?>
		</main>
	</div>
<?php get_footer();
