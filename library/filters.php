<?php
if (is_singular('lawyers')) {

	add_filter("wpcf7_posted_data", function ($wpcf7_posted_data) {
		$post = get_post($wpcf7_posted_data["_wpcf7_container_post"]);
		$wpcf7_posted_data["containerURL"] = get_permalink($post);

		return $wpcf7_posted_data;
	});

}
