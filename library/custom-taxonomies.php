<?php

add_action('init', function () {

	createCustomTax('sphere_of_law', 'lawyers', [
		'hierarchical' => true,
		'show_admin_column' => true,
		'labels' => [
			'name' => __('Сфера права', 'taxonomy general name', 'foundation'),
			'singular_name' => __('Сфера права', 'taxonomy singular name', 'foundation'),
			'menu_name' => __('Сфера права', 'foundation'),
		]
	]);

	createCustomTax('type_of_help', 'lawyers', [
		'hierarchical' => true,
		'show_admin_column' => true,
		'labels' => [
			'name' => __('Вид Допомоги', 'taxonomy general name', 'foundation'),
			'singular_name' => __('Вид Допомоги', 'taxonomy singular name', 'foundation'),
			'menu_name' => __('Вид Допомоги', 'foundation'),
		]
	]);

	createCustomTax('testimonial_cat', 'testimonials', [
		'hierarchical' => true,
		'show_admin_column' => true,
		'labels' => [
			'name' => __('Категорія', 'taxonomy general name', 'foundation'),
			'singular_name' => __('Категорія', 'taxonomy singular name', 'foundation'),
			'menu_name' => __('Категорії', 'foundation'),
		]
	]);

});


