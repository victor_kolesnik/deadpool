<?php
/**
 * Output HTML markup of template with passed args
 *
 * @param string $file File name without extension (.php)
 * @param array $args Array with args ($key=>$value)
 * @param string $default_folder Requested file folder
 *
 * */
function show_template($file, $args = null, $default_folder = 'template-parts')
{
	echo return_template($file, $args, $default_folder);
}

/**
 * Return HTML markup of template with passed args
 *
 * @param string $file File name without extension (.php)
 * @param array $args Array with args ($key=>$value)
 * @param string $default_folder Requested file folder
 *
 * @return string template HTML
 * */
function return_template($file, $args = null, $default_folder = 'template-parts')
{
	$file = $default_folder . '/' . $file . '.php';
	if ($args) {
		extract($args);
	}
	if (locate_template($file)) {
		ob_start();
		include(locate_template($file)); //Theme Check free. Child themes support.
		$template_content = ob_get_clean();

		return $template_content;
	}

	return '';
}

/**
 * Get Post Featured image
 *
 * @return string Post featured image url
 * @var string $size = 'full' featured image size
 *
 * @var int $id Post id
 */
function get_attached_img_url($id = 0, $size = "medium_large")
{
	$img = wp_get_attachment_image_src(get_post_thumbnail_id($id), $size);

	return $img[0];
}

/**
 * Dynamic admin function
 *
 * @return void
 * @var int $post_id Post id
 *
 * @var string $column_name Column id
 */
function template_detail_field_for_page($column_name, $post_id)
{
	if ($column_name == 'template') {
		$template_name = str_replace('.php', '', get_post_meta($post_id, '_wp_page_template', true));
		echo '<span style="text-transform: capitalize;">' . str_replace(array(
				'template-', '/'
			), '', substr($template_name, strpos($template_name, '/'), strlen($template_name))) . ' Page</span>';
	}

	return;
}

/**
 * Output background image style
 *
 * @param array|string $img Image array or url
 * @param string $size Image size to retrieve
 * @param bool $echo Whether to output the the style tag or return it.
 *
 * @return string|void String when retrieving.
 */
function bg($img, $size = '', $echo = true)
{

	if (!$img) {
		return;
	}

	if (is_array($img)) {
		$url = $size ? $img['sizes'][$size] : $img['url'];
	} else {
		$url = $img;
	}

	$string = 'style="background-image: url(' . $url . ')"';

	if ($echo) {
		echo $string;
	} else {
		return $string;
	}
}

/**
 * Format phone number, trim all unnecessary characters
 *
 * @param string $phone Phone number
 *
 * @return string Formatted phone number
 */
function preparePhone($phone)
{
	return preg_replace('/[^+\d]+/', '', $phone);
}

/**
 * Return/Output SVG as html
 *
 * @param array|string $img Image link or array
 * @param string $class Additional class attribute for img tag
 * @param string $size Image size if $img is array
 *
 * @return void
 */
function display_svg($img, $class = '', $size = 'medium', $attributes = false)
{
	echo returnSvg($img, $class, $size, $attributes);
}

function returnSvg($img, $class = '', $size = 'medium', $attributes = false)
{
	if (!$img) :
		return '';
	endif;
	$attr_array = array();
	if ($attributes) {
		foreach ($attributes as $attr_name => $attr_value) {
			$attr_array[] = $attr_name . '="' . $attr_value . '"';
		}
	}
	$icon_url = is_array($img) ? $img['url'] : $img;

	if (strpos($icon_url, ABSPATH) !== false) {
		$svgPath = $icon_url;
	} else {
		preg_match('/(wp-content\/uploads\/.*\.svg)/i', $icon_url, $matches);
		$svgPath = isset($matches[1]) ? ABSPATH . $matches[1] : null;
	}

	if ($svgPath) {
		$image = file_get_contents($svgPath);
		if ($attr_array) {
			$image = str_replace('<svg ', '<svg ' . implode(' ', $attr_array) . ' ', $image);
		}
		if ($class) {
			$image = str_replace('<svg ', '<svg class="' . $class . '" ', $image);
		}
	} elseif (is_array($img)) {
		$src = $img['sizes'][$size];
		$image = '<img class="' . $class . '" ' . implode(' ', $attr_array) . ' src="' . $src . '" />';
	} else {
		$image = '<img class="' . $class . '" ' . implode(' ', $attr_array) . ' src="' . $img . '" />';
	}

	return $image;
}

/*function show_custom_logo($size = 'medium')
{
	if ($custom_logo_id = get_theme_mod('custom_logo')) {
		$attachment_array = wp_get_attachment_image_src($custom_logo_id, $size);
		$logo_url = $attachment_array[0];
	} else {
		$logo_url = get_stylesheet_directory_uri() . '/images/custom-logo.png';
	}
	$logo_image = '<img src="' . $logo_url . '" class="custom-logo" itemprop="siteLogo" alt="' . get_bloginfo('name') . '">';
	$html = sprintf('<a href="%1$s" class="custom-logo-link" rel="home" title="%2$s" itemscope>%3$s</a>', esc_url(home_url('/')), get_bloginfo('name'), $logo_image);
	echo apply_filters('get_custom_logo', $html);
}*/


function registerPostType($slug, $singular, $plural, $args = [])
{
	register_post_type($slug, array_merge([
		'labels' => [
			'name' => $plural,
			'singular_name' => $singular,
			'add_new' => _x('Додати', 'backend: post type label', 'foundation'),
			'add_new_item' => sprintf(_x('Додати новий %s', 'backend: post type label', 'foundation'), $singular),
			'edit_item' => sprintf(_x('Редагувати %s', 'backend: post type label', 'foundation'), $singular),
			'new_item' => sprintf(_x('New %s', 'backend: post type label', 'foundation'), $singular),
			'view_item' => sprintf(_x('Переглянути %s', 'backend: post type label', 'foundation'), $singular),
			'search_items' => sprintf(_x('Search %s', 'backend: post type label', 'foundation'), $plural),
			'not_found' => sprintf(_x('No %s found', 'backend: post type label', 'foundation'), $singular),
			'not_found_in_trash' => sprintf(
				_x('No %s  in Trash', 'backend: post type label', 'foundation'),
				$plural
			),
			'parent_item_colon' => sprintf(_x('Parent %s:', 'backend: post type label', 'foundation'), $singular),
			'menu_name' => $plural,
		],
		'show_ui' => true
	], $args));
}

/**
 * Adds or overwrites a taxonomy
 * @param string $taxName title for taxonomy
 * @param string $cptName title of cpt for which taxonomy is created
 * @param array $args
 */
function createCustomTax($taxName, $cptName, $args = [])
{
	register_taxonomy($taxName, $cptName, array_merge([

		'hierarchical' => true,
		'labels' => [
			'name' => _x('Custom Category', 'taxonomy general name', 'foundation'),
			'singular_name' => _x('Category', 'taxonomy singular name', 'foundation'),
			'search_items' => __('Search category', 'foundation'),
			'all_items' => __('All categoris', 'foundation'),
			'parent_item' => __('Parent category', 'foundation'),
			'parent_item_colon' => __('Parent category:', 'foundation'),
			'edit_item' => __('Edit category', 'foundation'),
			'update_item' => __('Update category', 'foundation'),
			'add_new_item' => __('Add New category', 'foundation'),
			'new_item_name' => __('New category Name', 'foundation'),
			'menu_name' => __('Category', 'foundation'),
		],
	], $args));
}
