<?php


add_action('init', function () {
	registerPostType('lawyers', 'Юрист', 'Юристи', [
		'public' => true,
		'has_archive' => true,
		'show_in_nav_menus' => true,
//		'show_in_rest' => true,
		'show_ui' => true,
		'menu_icon' => 'dashicons-businessperson',
		'supports' => [
			'title',
			'editor',
			'thumbnail',
			'excerpt',
		],
	]);

	registerPostType('testimonials', 'Відгук', 'Відгуки', [
		'public' => true,
		'has_archive' => true,
		'show_in_nav_menus' => true,
//		'show_in_rest' => true,
		'show_ui' => true,
		'menu_icon' => 'dashicons-format-status',
		'supports' => [
			'title',
			'editor',
			'thumbnail',
			'excerpt',
		],
	]);
});
