<?php

function lawyers_filter()
{
	$city = isset($_POST['city']) ? $_POST['city'] : '';
	$sphere = isset($_POST['sphere']) ? $_POST['sphere'] : '';
	$help_type = isset($_POST['help-type']) ? $_POST['help-type'] : '';
	$experience = isset($_POST['experience']) ? $_POST['experience'] : '';
	$attorney = isset($_POST['attorney']) ? $_POST['attorney'] : '';
	$business_trip = isset($_POST['business-trip']) ? $_POST['business-trip'] : '';
	$offset = isset($_POST['offset']) ? $_POST['offset'] : 'no-data-offset';

	$lawyersArgs = [
		'post_type' => 'lawyers',
		'posts_per_page' => 18,
		'orderby' => 'date',
		'order' => 'ASC',
		'post_status' => 'publish',
		'offset' => $offset
	];

	if ($city) {
		$lawyersArgs['meta_query'][] = [
			'key' => 'city',
			'value' => $city,
		];
	}

	if ($experience) {
		$lawyersArgs['meta_query'][] = [
			'key' => 'experience',
			'value' => $experience,
		];
	}

	if ($attorney && $attorney == 'on') {
		$lawyersArgs['meta_query'][] = [
			'key' => 'attorney',
			'value' => true
		];
	}

	if ($business_trip && $business_trip == 'on') {
		$lawyersArgs['meta_query'][] = [
			'key' => 'business_trip',
			'value' => true
		];
	}


	if ($sphere && $help_type) {
		$lawyersArgs['tax_query'][] = [
			'relation' => 'AND',
			[
				'taxonomy' => 'sphere_of_law',
				'field' => 'id',
				'terms' => $sphere,
			],
			[
				'taxonomy' => "type_of_help",
				'field' => 'id',
				'terms' => $help_type,
			]
		];
	} else {
		if ($sphere) {
			$lawyersArgs['tax_query'][] = [
				'taxonomy' => "sphere_of_law",
				'field' => 'id',
				'terms' => $sphere,
			];
		}
		if ($help_type) {
			$lawyersArgs['tax_query'][] = [
				'taxonomy' => "type_of_help",
				'field' => 'id',
				'terms' => $help_type,
			];
		}
	}


//	var_dump($lawyersArgs);
	$lawyers = new WP_Query($lawyersArgs);
	?>

	<?php if ($lawyers->have_posts()): ?>
	<div class="preloader preloader--hidden">
		<div class="preloader__icon"></div>
	</div>
	<?php while ($lawyers->have_posts()): $lawyers->the_post() ?>
		<?php show_template('lawyer-card', ['columns' => '4', 'medium_columns' => '6']); ?>
	<?php endwhile; ?>
<?php endif; ?>

	<?php wp_reset_query(); ?>
	<?php
	wp_die();
}

add_action('wp_ajax_lawyers_filter', 'lawyers_filter');
add_action('wp_ajax_nopriv_lawyers_filter', 'lawyers_filter');


function ba_make_liqpay_form(){
	$public_key = 'sandbox_i72633043158';
	$private_key = 'sandbox_t7C7XDvt3hujBc07aARY93Q2EyPr920t2hFn85xJ';

	$price = $_POST['price'] ? $_POST['price'] : 100;
	$email = isset($_POST['email']) ? $_POST['email'] : '';

	$micro = sprintf("%06d",(microtime(true) - floor(microtime(true))) * 1000000); // Ну раз что-то нужно добавить для полной уникализации то ..

	$number = date("YmdHis"); //Все вместе будет первой частью номера ордера
	$order_id = $number.$micro; //Будем формировать номер ордера таким образом…

	$description = $email ? "Підтримка від " . $email : 'Підтримка Deadpool';

	$liqpay = new LiqPay($public_key, $private_key);

	$html = $liqpay->cnb_form(array(
		'action' => 'pay',
		'version' => '3',
		'amount' => "$price",
		'language' => 'uk',
		'currency' => 'UAH', //Можно менять 'EUR','UAH','USD','RUB','RUR'
		'description' => $description, //Или изменить на $desc
		'order_id' => $order_id
	));

	die($html);
}

add_action( 'wp_ajax_make_form', 'ba_make_liqpay_form' );
add_action( 'wp_ajax_nopriv_make_form', 'ba_make_liqpay_form' );

