<?php

add_image_size('full_hd', 1920, 1080, ['center', 'center']);
add_image_size('lawyer', 400, 400, ['center', 'center']);
add_image_size('lawyer-card', 224, 224, ['center', 'center']);
add_image_size('news', 312, 164, ['center', 'center']);
add_image_size('testimonial', 48, 48, ['center', 'center']);
add_image_size('news_gallery', 136, 76, ['center', 'center']);
