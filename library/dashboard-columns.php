<?php

function dashboard_columns($defaults)
{
	$defaults['city'] = 'Місто';
	$defaults['exp'] = 'Досвід';
	$defaults['attorney'] = 'Адвокат';
	$defaults['business_trip'] = 'Виїзд в райони та області';
	return $defaults;
}

function dashboard_columns_content($column_name, $post_ID)
{
	if ($column_name == 'city') {
		if ($city = get_field('city')) {
			echo $city;
		}
	}
	if ($column_name == 'exp') {
		if ($experience = get_field('experience')) {
			echo $experience;
		}
	}
	if ($column_name == 'attorney') {
		$attorney = get_field('attorney');
		if ($attorney == 'on') {
			echo 'Адвокат';
		} else {
			echo '';
		}
	}

	if ($column_name == 'business_trip') {
		$business_trip = get_field('business_trip');
		if ($business_trip == 'on') {
			echo 'Виїзд в райони та області';
		} else {
			echo '';
		}
	}
}

add_filter('manage_lawyers_posts_columns', 'dashboard_columns');
add_action('manage_lawyers_posts_custom_column', 'dashboard_columns_content', 10, 3);
