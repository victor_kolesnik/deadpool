<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "off-canvas-wrap" div and all content after.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */
?>

<?php show_template('support-section'); ?>

<?php if (is_front_page()): ?>
	<?php show_template('partners'); ?>
<?php endif ?>

<footer class="footer">
	<div class="row column">
		<div class="footer-grid">
			<?php dynamic_sidebar('footer-widgets'); ?>
		</div>
	</div>
	<div class="row flex-align-middle">
		<div class="large-6 medium-6 column">
			<div class="footer-info">
				<a class="logo" href="<?php echo esc_url(home_url('/')); ?>" rel="home">
					<?php if ($logo = get_field('logo', 'options')): ?>
						<img src="<?php echo $logo['sizes']['thumbnail'] ?>" alt="<?php echo $logo['alt'] ?>">
					<?php endif; ?>
					<span class="logo__title"><?php bloginfo('name'); ?></span>
				</a>
				<h6 class="footer-info__desc"><?php bloginfo('description'); ?></h6>
			</div>
		</div>
		<div class="large-6 medium-6 column">
			<iframe class="footer__subscribe" frameborder="0" src="https://secure.esputnik.com.ua/1N6FwGMLcOs" width="100%"
							height="100%"
							scrolling="no"></iframe>
		</div>
	</div>
	<hr>
	<div class="row column">
		<p class="footer__title">Ми у соц мережах</p>
	</div>
	<div class="row footer-bottom">
		<div class="medium-6 column">
			<?php if (have_rows('social_networks', 'options')) : ?>
				<div class="footer-socials">
					<?php while (have_rows('social_networks', 'options')) : the_row(); ?>

						<?php if ($facebook = get_sub_field('facebook')): ?>
							<a class="footer-socials__item" target="_blank"
								 href="<?php echo $facebook ?>">
								<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path
										d="M19.8571 2H4.14286C3.57454 2 3.02949 2.22576 2.62763 2.62763C2.22576 3.02949 2 3.57454 2 4.14286L2 19.8571C2 20.4255 2.22576 20.9705 2.62763 21.3724C3.02949 21.7742 3.57454 22 4.14286 22H10.2701V15.2004H7.45759V12H10.2701V9.56071C10.2701 6.78616 11.9219 5.25357 14.4518 5.25357C15.6634 5.25357 16.9304 5.46964 16.9304 5.46964V8.19286H15.5344C14.1589 8.19286 13.7299 9.04643 13.7299 9.92188V12H16.8004L16.3094 15.2004H13.7299V22H19.8571C20.4255 22 20.9705 21.7742 21.3724 21.3724C21.7742 20.9705 22 20.4255 22 19.8571V4.14286C22 3.57454 21.7742 3.02949 21.3724 2.62763C20.9705 2.22576 20.4255 2 19.8571 2Z"
										fill="white"/>
								</svg>
								<span>Facebook</span>
							</a>
						<?php endif; ?>

						<?php if ($twitter = get_sub_field('twitter')): ?>
							<a class="footer-socials__item" target="_blank"
								 href="<?php echo $twitter ?>">
								<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path
										d="M7.54667 21.9996C9.38771 22.013 11.213 21.651 12.9168 20.9346C14.6205 20.2183 16.1688 19.1619 17.4719 17.8267C18.775 16.4915 19.8069 14.904 20.5079 13.1563C21.2089 11.4086 21.565 9.53538 21.5556 7.64532C21.5556 7.42631 21.5556 7.2073 21.5556 6.99742C22.5134 6.27797 23.3407 5.39152 24 4.37841C23.102 4.78733 22.1491 5.05497 21.1733 5.17233C22.1987 4.54048 22.9661 3.54837 23.3333 2.37994C22.3693 2.96821 21.3139 3.38185 20.2133 3.60275C19.4723 2.78909 18.4902 2.24947 17.4197 2.06774C16.3492 1.88601 15.2502 2.07234 14.2934 2.59778C13.3367 3.12322 12.5757 3.95836 12.1289 4.97343C11.682 5.9885 11.5742 7.12667 11.8222 8.2111C9.86333 8.11317 7.94663 7.59194 6.1974 6.68147C4.44817 5.771 2.9058 4.49179 1.67111 2.92747C1.04098 4.04056 0.847417 5.35824 1.12976 6.6126C1.41211 7.86697 2.14917 8.96387 3.19111 9.6803C2.40915 9.65601 1.64417 9.44012 0.96 9.05064V9.11452C0.96161 10.2807 1.35516 11.4105 2.0742 12.3133C2.79324 13.216 3.79371 13.8364 4.90667 14.0696C4.18167 14.2703 3.42162 14.2984 2.68444 14.1518C3.00051 15.1564 3.61395 16.0347 4.43929 16.6645C5.26462 17.2942 6.26076 17.644 7.28889 17.6651C5.54567 19.0722 3.39089 19.8342 1.17333 19.8278C0.781063 19.8308 0.389048 19.8064 0 19.7548C2.2536 21.231 4.87339 22.0103 7.54667 21.9996Z"
										fill="white"/>
								</svg>
								<span>Twitter</span>
							</a>
						<?php endif; ?>

						<?php if ($youtube = get_sub_field('youtube')): ?>
							<a class="footer-socials__item" target="_blank"
								 href="<?php echo $youtube ?>">
								<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path
										d="M23.5019 6.22989C23.3646 5.72417 23.0979 5.26295 22.7281 4.89167C22.3582 4.52039 21.8981 4.25185 21.3929 4.11252C19.5108 3.59998 11.9991 3.59998 11.9991 3.59998C11.9991 3.59998 4.48749 3.59998 2.60538 4.09571C2.10048 4.24101 1.64137 4.51357 1.27208 4.88728C0.902786 5.26099 0.635691 5.7233 0.496404 6.22989C0.154519 8.13475 -0.0114253 10.067 0.000668833 12.0023C-0.0119494 13.9375 0.153999 15.8699 0.496404 17.7747C0.633729 18.2804 0.900434 18.7416 1.27024 19.1129C1.64005 19.4842 2.10021 19.7527 2.60538 19.892C4.5043 20.4046 11.9991 20.4046 11.9991 20.4046C11.9991 20.4046 19.5108 20.4046 21.3929 19.9088C21.8988 19.771 22.3598 19.503 22.7299 19.1315C23.0999 18.76 23.3661 18.2979 23.5019 17.7915C23.8448 15.8811 24.0108 13.9431 23.9976 12.0023C24.0222 10.0665 23.8561 8.13309 23.5019 6.22989ZM9.60449 15.5985V8.40609L15.8558 12.0023L9.60449 15.5985Z"
										fill="white"/>
								</svg>
								<span>Youtube</span>
							</a>
						<?php endif; ?>

						<?php if ($instagram = get_sub_field('instagram')): ?>
							<a class="footer-socials__item" target="_blank"
								 href="<?php echo $instagram ?>">
								<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path
										d="M11.9975 8.66525C10.1613 8.66525 8.66274 10.1638 8.66274 12C8.66274 13.8362 10.1613 15.3348 11.9975 15.3348C13.8337 15.3348 15.3323 13.8362 15.3323 12C15.3323 10.1638 13.8337 8.66525 11.9975 8.66525ZM21.9993 12C21.9993 10.6191 22.0118 9.25064 21.9342 7.87221C21.8567 6.27113 21.4914 4.85017 20.3206 3.67938C19.1473 2.50609 17.7289 2.14334 16.1278 2.06579C14.7469 1.98824 13.3784 2.00074 12 2.00074C10.6191 2.00074 9.25064 1.98824 7.87221 2.06579C6.27113 2.14334 4.85017 2.50859 3.67938 3.67938C2.50609 4.85267 2.14334 6.27113 2.06579 7.87221C1.98824 9.25314 2.00074 10.6216 2.00074 12C2.00074 13.3784 1.98824 14.7494 2.06579 16.1278C2.14334 17.7289 2.50859 19.1498 3.67938 20.3206C4.85267 21.4939 6.27113 21.8567 7.87221 21.9342C9.25314 22.0118 10.6216 21.9993 12 21.9993C13.3809 21.9993 14.7494 22.0118 16.1278 21.9342C17.7289 21.8567 19.1498 21.4914 20.3206 20.3206C21.4939 19.1473 21.8567 17.7289 21.9342 16.1278C22.0143 14.7494 21.9993 13.3809 21.9993 12ZM11.9975 17.131C9.15808 17.131 6.86653 14.8394 6.86653 12C6.86653 9.16058 9.15808 6.86903 11.9975 6.86903C14.8369 6.86903 17.1285 9.16058 17.1285 12C17.1285 14.8394 14.8369 17.131 11.9975 17.131ZM17.3386 7.8572C16.6757 7.8572 16.1403 7.32184 16.1403 6.65889C16.1403 5.99594 16.6757 5.46058 17.3386 5.46058C18.0016 5.46058 18.5369 5.99594 18.5369 6.65889C18.5371 6.81631 18.5063 6.97222 18.4461 7.1177C18.386 7.26317 18.2977 7.39535 18.1864 7.50666C18.0751 7.61798 17.9429 7.70624 17.7974 7.76639C17.6519 7.82654 17.496 7.8574 17.3386 7.8572Z"
										fill="white"/>
								</svg>
								<span>Instagram</span>
							</a>
						<?php endif; ?>

					<?php endwhile; ?>
				</div>
			<?php endif; ?>
		</div>
		<div class="medium-6 column">
			<?php if ($copyright = get_field('copyright', 'options')): ?>
				<p class="footer__title text-center medium-text-right"><?php echo $copyright ?><?php echo date(' Y') ?></p>
			<?php endif; ?>
		</div>
	</div>
</footer>

<?php if (get_theme_mod('wpt_mobile_menu_layout') === 'offcanvas') : ?>
	</div><!-- Close off-canvas content -->
	</div>
<?php endif; ?>

<?php wp_footer(); ?>
</body>
</html>
