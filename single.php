<?php
/**
 * Single
 *
 * Loop container for single post content
 */
$bg_img = get_attached_img_url(get_the_ID());
get_header(); ?>

	<div class="progress-container">
		<div class="progress-bar" id="myBar"></div>
	</div>

	<div class="main-container">
		<main>
			<?php if (have_posts()) : ?>
				<?php while (have_posts()) : the_post(); ?>
					<article class="single-news-article">
						<div class="row column">
							<?php $terms = wp_get_post_terms(get_the_ID(), 'category'); ?>
							<?php foreach ($terms as $term): ?>
								<span class="news-item__cat"><?php echo $term->name ?></span>
							<?php endforeach ?>
							<h1><?php the_title() ?></h1>
						</div>
						<div class="row">
							<div class="large-offset-1 large-9 column">
								<?php if ($bg_img): ?>
									<div class="post-featured-image"<?php bg($bg_img) ?>></div>
								<?php endif ?>
							</div>
						</div>
						<div class="article-text">
							<div class="row">
								<div class="large-offset-1 large-1 column">
									<div class="share-links">
										<a class="share-links__link" target="_blank"
											 href="https://www.facebook.com/sharer/sharer.php?&u=<?php echo urlencode(get_the_permalink()); ?>">
											<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
												<path
													d="M17.8571 0H2.14286C1.57454 0 1.02949 0.225765 0.627628 0.627628C0.225765 1.02949 0 1.57454 0 2.14286L0 17.8571C0 18.4255 0.225765 18.9705 0.627628 19.3724C1.02949 19.7742 1.57454 20 2.14286 20H8.27009V13.2004H5.45759V10H8.27009V7.56071C8.27009 4.78616 9.92188 3.25357 12.4518 3.25357C13.6634 3.25357 14.9304 3.46964 14.9304 3.46964V6.19286H13.5344C12.1589 6.19286 11.7299 7.04643 11.7299 7.92188V10H14.8004L14.3094 13.2004H11.7299V20H17.8571C18.4255 20 18.9705 19.7742 19.3724 19.3724C19.7742 18.9705 20 18.4255 20 17.8571V2.14286C20 1.57454 19.7742 1.02949 19.3724 0.627628C18.9705 0.225765 18.4255 0 17.8571 0Z"
													fill="#c4c7cc"/>
											</svg>
										</a>
										<a class="share-links__link" target="_blank"
											 href="https://twitter.com/intent/tweet?url=<?php echo urlencode(get_the_permalink()); ?>">
											<svg width="24" height="20" viewBox="0 0 24 20" fill="none" xmlns="http://www.w3.org/2000/svg">
												<path
													d="M7.54667 19.9996C9.38771 20.013 11.213 19.651 12.9168 18.9346C14.6205 18.2183 16.1688 17.1619 17.4719 15.8267C18.775 14.4915 19.8069 12.904 20.5079 11.1563C21.2089 9.40857 21.565 7.53538 21.5556 5.64532C21.5556 5.42631 21.5556 5.2073 21.5556 4.99742C22.5134 4.27797 23.3407 3.39152 24 2.37841C23.102 2.78733 22.1491 3.05497 21.1733 3.17233C22.1987 2.54048 22.9661 1.54837 23.3333 0.379942C22.3693 0.968213 21.3139 1.38185 20.2133 1.60275C19.4723 0.789088 18.4902 0.249471 17.4197 0.0677397C16.3492 -0.113992 15.2502 0.072339 14.2934 0.597781C13.3367 1.12322 12.5757 1.95836 12.1289 2.97343C11.682 3.9885 11.5742 5.12667 11.8222 6.2111C9.86333 6.11317 7.94663 5.59194 6.1974 4.68147C4.44817 3.771 2.9058 2.49179 1.67111 0.927469C1.04098 2.04056 0.847417 3.35824 1.12976 4.6126C1.41211 5.86697 2.14917 6.96387 3.19111 7.6803C2.40915 7.65601 1.64417 7.44012 0.96 7.05064V7.11452C0.96161 8.28066 1.35516 9.4105 2.0742 10.3133C2.79324 11.216 3.79371 11.8364 4.90667 12.0696C4.18167 12.2703 3.42162 12.2984 2.68444 12.1518C3.00051 13.1564 3.61395 14.0347 4.43929 14.6645C5.26462 15.2942 6.26076 15.644 7.28889 15.6651C5.54567 17.0722 3.39089 17.8342 1.17333 17.8278C0.781063 17.8308 0.389048 17.8064 0 17.7548C2.2536 19.231 4.87339 20.0103 7.54667 19.9996Z"
													fill="#c4c7cc"/>
											</svg>
										</a>
										<a class="share-links__link" target="_blank"
											 href="https://telegram.me/share/url?url=<?php echo urlencode(get_the_permalink()); ?>">
											<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
												<circle cx="9.58325" cy="10.4166" r="6.25" fill="white"/>
												<path
													d="M10.0001 1.66663C5.39696 1.66663 1.66675 5.39683 1.66675 9.99996C1.66675 14.6031 5.39696 18.3333 10.0001 18.3333C14.6032 18.3333 18.3334 14.6031 18.3334 9.99996C18.3334 5.39683 14.6032 1.66663 10.0001 1.66663ZM14.0949 7.37913L12.7272 13.8208C12.6324 14.2802 12.3563 14.3875 11.972 14.176L9.88862 12.6364L8.88967 13.6093C8.773 13.7229 8.68133 13.8177 8.473 13.8177C8.20321 13.8177 8.248 13.7166 8.15737 13.4593L7.448 11.1291L5.38758 10.4875C4.94175 10.351 4.93862 10.0448 5.48862 9.82704L13.5157 6.72809C13.8803 6.56246 14.2355 6.81559 14.0949 7.37913Z"
													fill="#C4C7CC"/>
											</svg>
										</a>
										<button data-tooltip tabindex="2" data-position="bottom" data-alignment="center"
														class="copy-btn share-links__link" data-clipboard-text="<?php echo get_the_permalink() ?>">
											<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
												<path
													d="M6.5 11.4167H13.8333V9.58333H6.5V11.4167ZM2.74167 10.5C2.74167 8.39167 4.39167 6.74167 6.5 6.74167H9.25V5H6.5C3.475 5 1 7.475 1 10.5C1 13.525 3.475 16 6.5 16H9.25V14.2583H6.5C4.39167 14.2583 2.74167 12.6083 2.74167 10.5ZM13.8333 5H11.0833V6.74167H13.8333C15.9417 6.74167 17.5917 8.39167 17.5917 10.5C17.5917 12.6083 15.9417 14.2583 13.8333 14.2583H11.0833V16H13.8333C16.8583 16 19.3333 13.525 19.3333 10.5C19.3333 7.475 16.8583 5 13.8333 5Z"
													fill="#C4C7CC"/>
											</svg>
											<span class="copy-btn__tip">Cкопійовано</span>
										</button>
									</div>
								</div>
								<div class="large-8 column end">
									<?php the_content(); ?>
								</div>
							</div>
						</div>
					</article>
				<?php endwhile; ?>
			<?php endif; ?>
			<section class="other-news-section">
				<div class="row column">
					<div class="title-and-link">
						<h2>Читайте також</h2>
						<a class="title-and-link__link" href="<?php echo get_permalink(get_option('page_for_posts')); ?>">
							Всі новини
						</a>
					</div>
				</div>
				<div class="row">
					<?php
					$id = get_the_ID();
					$postsArgs = array(
						'post_type' => 'post',
						'posts_per_page' => 3,
						'orderby' => 'date',
						'order' => 'ASC',
						'post__not_in' => [$id]
					);
					$posts = new WP_Query($postsArgs); ?>
					<?php if ($posts->have_posts()): ?>
						<?php while ($posts->have_posts()): $posts->the_post() ?>
							<?php show_template('content'); ?>
						<?php endwhile; ?>
					<?php endif; ?>

				</div>
			</section>
		</main>
	</div>
<?php get_footer();
