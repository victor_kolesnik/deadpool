<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "container" div.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>
<!doctype html>
<html class="no-js" <?php language_attributes(); ?> >
<head>
	<meta charset="<?php bloginfo('charset'); ?>"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<?php wp_head(); ?>
</head>
<?php

if (is_front_page() || is_page_template('page-templates/template-about.php')) {
	$bg_color = 'black-page';
}
?>
<body <?php body_class($bg_color); ?>>

<?php if (get_theme_mod('wpt_mobile_menu_layout') === 'offcanvas') : ?>
	<?php get_template_part('template-parts/mobile-off-canvas'); ?>
<?php endif; ?>

<header class="site-header" role="banner">
	<div class="site-title-bar title-bar" <?php foundationpress_title_bar_responsive_toggle(); ?>>
		<div class="title-bar-left">
			<button aria-label="<?php _e('Main Menu', 'foundationpress'); ?>" class="menu-icon" type="button"
							data-toggle="<?php foundationpress_mobile_menu_id(); ?>"></button>
			<span class="site-mobile-title title-bar-title">
				<a class="logo" href="<?php echo esc_url(home_url('/')); ?>" rel="home">
					<?php if ($logo = get_field('logo', 'options')): ?>
						<img src="<?php echo $logo['sizes']['thumbnail'] ?>" alt="<?php echo $logo['alt'] ?>">
					<?php endif; ?>
					<span class="logo__title hide-for-small-only"><?php bloginfo('name'); ?></span>
			</a>
			</span>
		</div>
	</div>

	<nav class="site-navigation top-bar site-header__nav hide-for-small-only" role="navigation"
			 id="<?php foundationpress_mobile_menu_id(); ?>">
		<div class="top-bar-left">
			<div class="site-desktop-title top-bar-title logo">
				<a class="logo" href="<?php echo esc_url(home_url('/')); ?>" rel="home">
					<?php if ($logo = get_field('logo', 'options')): ?>
						<img src="<?php echo $logo['sizes']['thumbnail'] ?>" alt="<?php echo $logo['alt'] ?>">
					<?php endif; ?>
					<span class=" logo__title"><?php bloginfo('name'); ?></span>
				</a>
			</div>
		</div>
		<div class="top-bar-right">
			<?php foundationpress_top_bar_r(); ?>

			<?php if (!get_theme_mod('wpt_mobile_menu_layout') || get_theme_mod('wpt_mobile_menu_layout') === 'topbar') : ?>
				<?php get_template_part('template-parts/mobile-top-bar'); ?>
			<?php endif; ?>
		</div>
		<button class="button js-getSupport">Підтримати проект</button>
	</nav>
</header>

<?php show_template('modals'); ?>
