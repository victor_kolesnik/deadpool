<?php
/**
 * The template for single lawyer
 */

get_header(); ?>

	<div class="main-container">
		<main>
			<?php while (have_posts()) : the_post(); ?>
				<?php show_template('lawyers-content'); ?>
			<?php endwhile; ?>

			<?php
			$id = get_the_ID();
			$lawyersArgs = [
				'post_type' => 'lawyers',
				'posts_per_page' => 8,
				'orderby' => 'rand',
				'order' => 'ACS',
				'post__not_in' => [$id]
			];
			$lawyers = new WP_Query($lawyersArgs); ?>
			<?php if ($lawyers->have_posts()): ?>
				<div class="row lawyers-grid">

					<div class="column">
						<div class="title-and-link">
							<h2>Інші юристи</h2>
							<a class="title-and-link__link" href="<?php echo get_permalink(62) ?>">Всі юристи</a>
						</div>
					</div>

					<?php while ($lawyers->have_posts()): $lawyers->the_post() ?>
						<?php show_template('lawyer-card', ['columns' => '3', 'medium_columns' => '4']); ?>
					<?php endwhile; ?>

				</div>
			<?php endif; ?>

		</main>
	</div>
<?php get_footer();
