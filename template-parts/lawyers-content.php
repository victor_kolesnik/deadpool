<article id="post-<?php the_ID(); ?>" <?php post_class('lawyer-article'); ?>>
	<div class="row">
		<div class="medium-6 column">
			<div class="lawyer">
				<?php if (has_post_thumbnail()): ?>
					<div class="lawyer__photo"><?php the_post_thumbnail('lawyer'); ?></div>
				<?php endif ?>
				<h2 class="lawyer__title"><?php the_title() ?></h2>
				<button id="getHelp" class="button button--big">Зв’язатись</button>
			</div>
		</div>

		<div class="medium-6 column">

			<div class="lawyer-info">
				<h6 class="lawyer-info__title">Інформація</h6>

				<ul class="lawyer-info__list">
					<?php if ($city = get_field('city')): ?>

						<li><?php echo $city ?></li>
					<?php endif; ?>

					<?php if ($experience = get_field('experience')): ?>
						<li><span>Досвід: </span><?php echo $experience ?> р.</li>

						<?php $terms = wp_get_post_terms(get_the_ID(),'sphere_of_law'); ?>

						<?php foreach ($terms as $term): ?>
							<li><?php echo $term->name ?></li>
						<?php endforeach ?>

					<?php endif; ?>
				</ul>

				<h6 class="lawyer-info__title">Місце роботи</h6>

				<?php if (have_rows('work_places')) : ?>

					<ul class="lawyer-info__list">
						<?php while (have_rows('work_places')) : the_row(); ?>
							<?php if ($name = get_sub_field('name')): ?>
								<li><?php echo $name ?></li>
							<?php endif; ?>
						<?php endwhile; ?>
					</ul>

				<?php endif; ?>

				<h6 class="lawyer-info__title">Освіта</h6>

				<?php if (have_rows('education')) : ?>

					<ul class="lawyer-info__list">
						<?php while (have_rows('education')) : the_row(); ?>
							<?php if ($name = get_sub_field('name')): ?>
								<li><?php echo $name ?></li>
							<?php endif; ?>
						<?php endwhile; ?>
					</ul>

				<?php endif; ?>
				<div class="lawyer-info__content"><?php the_content(); ?></div>
			</div>
		</div>
	</div>
</article>

