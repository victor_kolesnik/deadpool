<section class="support-section">
	<div class="row flex-align-middle">
		<div class="large-7 medium-6 column">
			<?php if ($support_text = get_field('support_text', 'options')): ?>
				<div class="support-section__text"><?php echo $support_text ?></div>
			<?php endif; ?>
		</div>
		<div class="large-5 medium-6 column">
			<form id="footer-support-form" method="POST" action="https://www.liqpay.ua/api/3/checkout" accept-charset="utf-8">
				<input type="hidden" name="data"
							 value="eyJwdWJsaWNfa2V5IjoiaTAwMDAwMDAwIiwidmVyc2lvbiI6IjMiLCJhY3Rpb24iOiJwYXkiLCJhbW91bnQiOiIzIiwiY3VycmVuY3kiOiJVQUgiLCJkZXNjcmlwdGlvbiI6InRlc3QiLCJvcmRlcl9pZCI6IjAwMDAwMSJ9"/>
				<input type="hidden" name="signature" value="wR+UZDC4jjeL/qUOvIsofIWpZh8="/>
				<div class="support-form">
					<div class="inputs-container">
						<input type="number" name="price" placeholder="200 грн.">
						<button type="submit" class="button">Підтримати</button>
					</div>
				</div>
			</form>
			<div class="hide" id="footer-liqpay-form"></div>
		</div>
	</div>
</section>
