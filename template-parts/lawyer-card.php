<?php
/**
 * @var string $columns
 * @var string $medium_columns
 */
?>
<div class="large-<?php echo $columns ?> medium-<?php echo $medium_columns ?> small-12 column">
	<a href="<?php the_permalink(); ?>" class="lawyer-card">
		<?php if (has_post_thumbnail()): ?>
			<div class="lawyer-card__img">
				<?php the_post_thumbnail('lawyer-card'); ?>
			</div>
		<?php endif ?>
		<h6 class="lawyer-card__title"><?php the_title() ?></h6>
		<?php $terms = wp_get_post_terms(get_the_ID(), 'sphere_of_law'); ?>
		<?php foreach ($terms as $term): ?>
			<p class="lawyer-card__sphere"><?php echo $term->name ?></p>
		<?php endforeach ?>

		<?php if ($experience = get_field('experience')): ?>
			<span class="lawyer-card__about">Досвід <?php echo $experience ?>р.</span>
		<?php endif; ?>

		<?php if ($city = get_field('city')): ?>
			<span class="lawyer-card__about"><?php echo $city ?></span>
		<?php endif; ?>
	</a>
</div>
