<?php
/**
 * Template part for off canvas menu
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>

<nav class="mobile-off-canvas-menu off-canvas position-left" id="<?php foundationpress_mobile_menu_id(); ?>"
		 data-off-canvas data-auto-focus="false" role="navigation">
	<a class="mobile-off-canvas-menu__logo logo" href="<?php echo esc_url(home_url('/')); ?>" rel="home">
		<?php if ($logo = get_field('logo', 'options')): ?>
			<img src="<?php echo $logo['sizes']['thumbnail'] ?>" alt="<?php echo $logo['alt'] ?>">
		<?php endif; ?>
		<span class=" logo__title"><?php bloginfo('name'); ?></span>
	</a>
	<?php foundationpress_mobile_nav(); ?>
	<p class="mobile-off-canvas-menu__desc"><?php bloginfo('description'); ?></p>
	<button class="mobile-off-canvas-menu__help button js-getSupport">Підтримати проект</button>
	<button class="mobile-off-canvas-menu__close">
		<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
			<path d="M7 7L17 17M17 7L7 17" stroke="#111111" stroke-width="2"/>
		</svg>
	</button>
</nav>
<div class="off-canvas-wrapper">
	<div class="off-canvas-content" data-off-canvas-content>
