<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */
$bg_img = get_attached_img_url(get_the_ID());
?>

<div class="medium-4 column">
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<a href="<?php the_permalink() ?>" class="news-item">
			<div class="news-item__img" <?php bg($bg_img, 'medium') ?>></div>
			<?php $terms = wp_get_post_terms(get_the_ID(), 'category'); ?>
			<?php foreach ($terms as $term): ?>
				<span class="news-item__cat"><?php echo $term->name ?></span>
			<?php endforeach ?>
			<h5 class="news-item__title">
				<?php the_title(); ?>
			</h5>
			<div class="news-item__date">
				<span><?php echo get_the_date(); ?></span>
				<span><?php echo get_the_time(); ?></span>
			</div>
		</a>
	</article>
</div>
