<div class="gallery-section">
	<?php
	$images = get_field('gallery');
	if ($images): ?>
		<div class="news-slider">
			<?php foreach ($images as $image): ?>
				<div class="news-slider__slide" <?php bg($image['sizes']['full_hd']) ?>></div>
			<?php endforeach; ?>
		</div>
	<?php endif; ?>

	<?php if ($images): ?>
		<div class="news-slider-nav">
			<?php foreach ($images as $image): ?>
				<div class="news-slider-nav__slide text-center">
					<img src="<?php echo $image['sizes']['news_gallery']; ?>" alt="<?php echo $image['alt']; ?>"/>
				</div>
			<?php endforeach; ?>
		</div>
	<?php endif; ?>
</div>


