<?php
/**
 * @var string $title
 */
?>

<section class="testimonials-section">
	<div class="row column">
		<h2 class="testimonials-section__title"><?php echo $title ?></h2>
	</div>
	<?php
	$testimonials = get_field('testimonials', 'options');
	if ($testimonials): ?>
		<div class="row expanded">
			<div class="testimonials-slider">
				<?php foreach ($testimonials as $post): ?>
					<?php setup_postdata($post); ?>
					<div class="medium-6 column">
						<div class="testimonials-slide">
							<div class="testimonial-top">
								<?php echo get_the_post_thumbnail($post->ID, 'testimonial'); ?>
								<div>
									<h6 class="testimonial-top__title">
										<?php echo get_the_title($post->ID); ?>
									</h6>
									<?php $terms = wp_get_post_terms($post->ID, 'testimonial_cat'); ?>
									<?php foreach ($terms as $term): ?>
										<span class="testimonial-top__cat">
													<?php echo $term->name ?>
												</span>
									<?php endforeach ?>
								</div>
							</div>
							<div><?php the_content(); ?></div>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
		<?php wp_reset_postdata(); ?>
	<?php else : ?>
		<?php
		$testimonialsArgs = [
			'post_type' => 'testimonials',
			'posts_per_page' => -1,
			'orderby' => 'date',
			'order' => 'ASC'
		];
		$testimonials = new WP_Query($testimonialsArgs); ?>
		<?php if ($testimonials->have_posts()): ?>
			<div class="row expanded">
				<div class="testimonials-slider">
					<?php while ($testimonials->have_posts()): $testimonials->the_post() ?>
						<div class="medium-6 column">
							<div class="testimonials-slide">
								<div class="testimonial-top">
									<?php if (has_post_thumbnail()): ?>
										<?php the_post_thumbnail('testimonial'); ?>
									<?php endif ?>
									<div>
										<h6 class="testimonial-top__title">
											<?php echo get_the_title(); ?>
										</h6>
										<?php $terms = wp_get_post_terms(get_the_ID(), 'testimonial_cat'); ?>
										<?php foreach ($terms as $term): ?>
											<span class="testimonial-top__cat">
													<?php echo $term->name ?>
												</span>
										<?php endforeach ?>
									</div>
								</div>
								<div><?php the_content(); ?></div>
							</div>
						</div>
					<?php endwhile; ?>
				</div>
			</div>
		<?php endif; ?>
	<?php endif; ?>
	<?php wp_reset_query(); ?>
</section>
