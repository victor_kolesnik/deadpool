<section class="partners-section">
	<?php if ($partners_title = get_field('partners_title', 'options')): ?>
		<div class="row column">
			<h2 class="partners-section__title"><?php echo $partners_title ?></h2>
		</div>
	<?php endif; ?>
	<?php if (have_rows('partners', 'options')) : ?>
		<div class="row column">
			<div class="partners">
				<?php while (have_rows('partners', 'options')) : the_row(); ?>
					<?php if ($image = get_sub_field('image')): ?>
						<a href="<?php echo $url = get_sub_field('link') ? $url : 'javascript:void(0)' ?>"
							 class="partners__partner">
							<img src="<?php echo $image['sizes']['medium'] ?>" alt="<?php echo $image['alt'] ?>">
						</a>
					<?php endif; ?>
				<?php endwhile; ?>
			</div>
		</div>
	<?php endif; ?>
</section>
