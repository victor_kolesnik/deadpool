<div class="modal-wrapper modal-wrapper--help">
	<div class="modal modal--help">
		<button class="modal__close-btn">
			<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
				<path d="M7 7L17 17M17 7L7 17" stroke="#111111" stroke-width="2"/>
			</svg>
		</button>
		<div class="help-form">
			<?php echo do_shortcode('[contact-form-7 id="220" title="Отримайте юридичну допомогу"]') ?>
		</div>
	</div>
</div>

<div class="modal-wrapper modal-wrapper--support">
	<div class="modal modal--support">
		<button class="modal__close-btn">
			<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
				<path d="M7 7L17 17M17 7L7 17" stroke="#111111" stroke-width="2"/>
			</svg>
		</button>
		<form class="support-modal-form" id="support-form" method="POST" action="https://www.liqpay.ua/api/3/checkout"
					accept-charset="utf-8">
			<input type="hidden" name="data"
						 value="eyJwdWJsaWNfa2V5IjoiaTAwMDAwMDAwIiwidmVyc2lvbiI6IjMiLCJhY3Rpb24iOiJwYXkiLCJhbW91bnQiOiIzIiwiY3VycmVuY3kiOiJVQUgiLCJkZXNjcmlwdGlvbiI6InRlc3QiLCJvcmRlcl9pZCI6IjAwMDAwMSJ9"/>
			<input type="hidden" name="signature" value="wR+UZDC4jjeL/qUOvIsofIWpZh8="/>
			<?php if ($support_modal_text = get_field('support_modal_text', 'options')): ?>
				<div class="support-modal-form__text"><?php echo $support_modal_text ?></div>
			<?php endif; ?>
			<div class="flex-wrapper">
				<input type="hidden" name="price" min="0" value="100">
				<div class="support-form-label">
					<input type="radio" checked name="price-radio" id="price100" value="100" class="hide"/>
					<label class="support-form-label__radio" for="price100">100 грн</label>
				</div>
				<div class="support-form-label support-form-label--mod">
					<input type="radio" name="price-radio" id="price200" value="200" class="hide"/>
					<label class="support-form-label__radio" for="price200">200 грн</label>
				</div>
				<div class="support-form-input">
					<input type="number" class="support-form-input__sum-input" placeholder="Інша сума" min="0" data-input-price>
				</div>
				<button type="submit" class="button">Підтримати Bihus.info</button>
			</div>
		</form>
		<div class="hide" id="liqpay-form"></div>
	</div>
</div>
