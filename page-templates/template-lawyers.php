<?php
/*
Template Name: Юристи
*/
$filterArgs = [
	'post_type' => 'lawyers',
	'posts_per_page' => -1,
	'orderby' => 'menu_order',
	'order' => 'ASC',
	'post_status' => 'publish'
];
$filter = new WP_Query($filterArgs);
$years = [];
$cities = [];
$i = 0;
if ($filter->have_posts()):
	while ($filter->have_posts()): $filter->the_post();

		if ($city = get_field('city')) {
			$cities[] = $city;
		}

		if ($experience = get_field('experience')) {
			$years[] = (int)$experience;
		}
		$i++;
	endwhile;
endif;
$years = array_unique($years);
$cities = array_unique($cities);
sort($years);
sort($cities);
wp_reset_query();
get_header(); ?>

	<div class="main-container">
		<main>
			<div class="row column">
				<div class="title-filter">
					<h1 class="main-title"><?php the_title() ?></h1>
					<button class="filter-button" hidden>
						<svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
							<path d="M6.66667 12H9.33333V10.6667H6.66667V12ZM2 4V5.33333H14V4H2ZM4 8.66667H12V7.33333H4V8.66667Z"
										fill="white"/>
						</svg>
						<span>Фільтр</span>
					</button>
				</div>
			</div>
			<div class="row">
				<div class="large-3 medium-4 column">
					<form class="lawyers-filter" id="lawyers-filter" action="" method="GET">
						<select class="lawyers-filter__select" name="city" id="city">
							<option value="">Місто</option>
							<?php foreach ($cities as $city): ?>
								<option value="<?php echo $city ?>"><?php echo $city ?></option>
							<?php endforeach ?>
						</select>
						<select class="lawyers-filter__select" name="sphere" id="sphere">
							<option value="">Сфера права</option>
							<?php $terms = get_terms('sphere_of_law', ['hide_empty' => true]); ?>

							<?php foreach ($terms as $term): ?>
								<option value="<?php echo $term->term_id ?>"><?php echo $term->name ?></option>
							<?php endforeach ?>

						</select>
						<select class="lawyers-filter__select" name="help-type" id="help-type">
							<option value="">Вид допомоги</option>
							<?php $terms = get_terms('type_of_help', ['hide_empty' => true]); ?>

							<?php foreach ($terms as $term): ?>
								<option value="<?php echo $term->term_id ?>"><?php echo $term->name ?></option>
							<?php endforeach ?>

						</select>
						<select class="lawyers-filter__select" name="experience" id="experience">
							<option value="">Досвід</option>

							<?php foreach ($years as $year): ?>
								<option value="<?php echo $year ?>"><?php echo $year ?>р.</option>
							<?php endforeach ?>

						</select>
						<div class="lawyers-filter__checkbox">
							<input type="checkbox" id="attorney" name="attorney">
							<label for="attorney">Адвокат</label>
						</div>
						<div class="lawyers-filter__checkbox">
							<input type="checkbox" id="business-trip" name="business-trip">
							<label for="business-trip">Виїзд в райони та області</label>
						</div>
						<button class="button" type="submit">Застосувати</button>
						<input type="hidden" name="action" value="lawyers_filter">
					</form>
				</div>
				<div class="large-9 medium-8 column">
					<?php
					$lawyersArgs = [
						'post_type' => 'lawyers',
						'posts_per_page' => 18,
						'orderby' => 'date',
						'order' => 'ASC',
						'post_status' => 'publish'
					];
					$lawyers = new WP_Query($lawyersArgs); ?>
					<?php if ($lawyers->have_posts()): ?>
						<div class="row lawyers-grid js-res">
							<div class="preloader preloader--hidden">
								<div class="preloader__icon"></div>
							</div>
							<?php while ($lawyers->have_posts()): $lawyers->the_post() ?>
								<?php show_template('lawyer-card', ['columns' => '4', 'medium_columns' => '6']); ?>
							<?php endwhile; ?>
						</div>
					<?php endif; ?>
					<?php wp_reset_query(); ?>

					<?php if ($i > 18): ?>
						<div class="row column text-center">
							<button class="button ajax-load-more">
								Показати ще
							</button>
							<svg hidden id="ajax-loader" width="54px" height="54px" xmlns="http://www.w3.org/2000/svg"
									 viewBox="0 0 100 100"
									 preserveAspectRatio="xMidYMid" class="lds-dual-ring" style="background: none;">
								<circle cx="50" cy="50" ng-attr-r="{{config.radius}}" ng-attr-stroke-width="{{config.width}}"
												ng-attr-stroke="{{config.stroke}}" ng-attr-stroke-dasharray="{{config.dasharray}}" fill="none"
												stroke-linecap="round" r="40" stroke-width="3" stroke="#ff727d"
												stroke-dasharray="62.83185307179586 62.83185307179586" transform="rotate(245.918 50 50)">
									<animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 50;360 50 50"
																		keyTimes="0;1" dur="1s" begin="0s" repeatCount="indefinite"></animateTransform>
								</circle>
							</svg>
						</div>
					<?php endif ?>

				</div>
			</div>
		</main>
	</div>
<?php get_footer();
