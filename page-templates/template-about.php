<?php
/*
Template Name:Про нас
*/
get_header(); ?>

	<main>
		<div class="main-container">
			<?php if (have_rows('faqs')) : ?>
				<section class="faqs-section">
					<div class="row">
						<div class="medium-8 column">
							<?php while (have_rows('faqs')) : the_row(); ?>
								<div class="faq">
									<?php if ($title = get_sub_field('title')): ?>
										<h3 class="faq__title"><?php echo $title ?></h3>
									<?php endif; ?>
									<?php if ($text = get_sub_field('text')): ?>
										<div><?php echo $text ?></div>
									<?php endif; ?>
									<hr>
								</div>
							<?php endwhile; ?>
						</div>
					</div>
				</section>
			<?php endif; ?>
		</div>
		<?php show_template('testimonials', ['title' => 'Які результати?']); ?>
	</main>

<?php get_footer();
