$(document).ready(function () {

  function lawyersFilter(options) {
    $.ajax({
      url: ajaxParams.url,
      type: 'POST',
      action: 'lawyers_filter',
      data: options,

      beforeSend: function () {
        $('.preloader').removeClass('preloader--hidden');
      },
      complete: function () {
        $('.preloader').addClass('preloader--hidden');
      },
      success: function (res) {
        if (!$.trim(res)) {
          $('.js-res').html('<p>Відсутні дані для відображення</p><div class="preloader preloader--hidden"><div class="preloader__icon"></div></div>');

        } else {
          $('.js-res').html(res);
        }
      }
    });
  }


  $('#lawyers-filter').on('submit', function (e) {
    e.preventDefault();

    const formData = $(this).serializeArray();
    lawyersFilter(formData);
  });


//AJAX posts loading
  $(document).on('click', '.ajax-load-more', function () {
    let formData = $('#lawyers-filter').serializeArray();
    let button = $(this);

    let offset = {
      name: "offset",
      value: $('.lawyer-card').length
    };

    formData = [...formData, offset];
    button.hide();
    $('#ajax-loader').show();

    $.ajax({
      url: ajaxParams.url,
      type: 'POST',
      action: 'lawyers_filter',
      data: formData,
      offset: offset,

      success: function (res) {
        $('#ajax-loader').hide();
        if (!$.trim(res)) {
          button.hide()
        } else {
          button.show()
        }

        $('.js-res').append(res);
      },
    });
  });
});
