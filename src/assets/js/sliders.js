import 'slick-carousel'

$(document).ready(function () {
  $('.testimonials-slider').slick({
    arrows: true,
    dots: false,
    slidesToShow: 2,
    slidesToScroll: 1,
    centerMode: true,
    centerPadding: '150px',
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          centerPadding: '50px',
          slidesToShow: 2,
          slidesToScroll: 1,
          arrows: false,
        }
      },
      {
        breakpoint: 600,
        settings: {
          centerPadding: '25px',
          slidesToShow: 1,
          slidesToScroll: 1,
          arrows: false,
        }
      },
      {
        breakpoint: 480,
        settings: {
          centerPadding: '25px',
          slidesToShow: 1,
          slidesToScroll: 1,
          arrows: false,
        }
      }
    ]
  });

  $('.news-slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.news-slider-nav'
  });

  $('.news-slider-nav').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    arrows: true,
    asNavFor: '.news-slider',
    dots: false,
    focusOnSelect: true
  });

});
