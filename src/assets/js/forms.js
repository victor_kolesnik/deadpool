$(document).ready(function () {
  const supportForm = $('#support-form');
  const footerSupportForm = $('#footer-support-form');

  let priceInput = $('[name="price"]');
  let priceRadio = $('[name="price-radio"]');

  $('.support-form-label__radio').on('click', function () {
    $('.support-form-label__radio').removeClass('active');
    $(this).addClass('active');
  });

  priceRadio.on('change', function (e) {

    let price = priceRadio.filter(':checked').val();

    priceInput.val(price);
  });

  $('[data-input-price]').on('change input', function (e) {
    $('.support-form-label__radio').removeClass('active');
    priceRadio.prop('checked', false);
    let price = 100;
    if ($(this).val() > 0) {
      priceRadio.prop('checked', false);

      price = $(this).val();
    } else {
      priceRadio.eq(0).prop('checked', true);
    }

    priceInput.val(price);
  });

  function supporrtForm(formId) {
    formId.on('submit', function (e) {
      e.preventDefault();
      let form = $(this);

      const data = {
        action: 'make_form',
        price: form.find('[name="price"]').val(),
      };

      $.post(ajaxParams, data, function (response) {

        let formWrap = $('#footer-liqpay-form');

        formWrap.html(response);
        formWrap.find('form').submit();
      });
    });
  }

  supporrtForm(supportForm);
  supporrtForm(footerSupportForm);

});
