import $ from 'jquery';
import './ajax-filter';
import './forms';
import './sliders';
import ClipboardJS from 'clipboard';
import whatInput from 'what-input';

window.$ = $;

import Foundation from 'foundation-sites';
// If you want to pick and choose which modules to include, comment out the above and uncomment
// the line below
//import './lib/foundation-explicit-pieces';

$(document).foundation();


function sctickyHeader() {
  const $header = $('.site-header');
  $('body').css('padding-top', $header.outerHeight(true));
  $('.progress-container').css('top', $header.outerHeight(true))
}

function allNewsProgressBar() {
  const $header = $('.site-header');
  $('.pace-progress').css('top', $header.outerHeight(true));
}

function newsProgressBar() {
  if ($('#myBar').length) {
    const winScroll = document.body.scrollTop || document.documentElement.scrollTop;
    const height = $('.single-news-article').outerHeight(true);
    const scrolled = (winScroll / height) * 100;
    document.getElementById("myBar").style.width = scrolled + "%";
  }
}

function counter() {
  $('.counter-item__number').each(function () {
    const $this = $(this),
      countTo = $this.attr('data-count');

    $({countNum: $this.text()}).animate({
        countNum: countTo
      },
      {
        duration: 3000,
        easing: 'linear',
        step: function () {
          $this.text(Math.floor(this.countNum));
        },
        complete: function () {
          $this.text(this.countNum);
        }
      });
  });
}

function clipText() {
  const clipboard = new ClipboardJS('.copy-btn');

  clipboard.on('success', function (e) {
    $('.copy-btn__tip').fadeIn();
    setTimeout(() => $('.copy-btn__tip').fadeOut(), 1500);
  });
}

$(document).ready(function () {

  sctickyHeader();
  clipText();
  counter();

  //filter on mobile
  $('.filter-button').on('click', function ($lawyersFilter, $newsFilter) {
    $lawyersFilter = $('.lawyers-filter');
    $newsFilter = $('#alm-filters-bihusnews');

    if ($lawyersFilter.length) {
      $lawyersFilter.slideToggle();
    }

    if ($newsFilter.length) {
      $newsFilter.slideToggle();
    }

  });

  // Modal Windows
  $('#getHelp').on('click', function () {
    $('.modal-wrapper--help').fadeIn();
  });

  $('.modal__close-btn').on('click', function () {
    $('.modal-wrapper').fadeOut();
  });

  $('.js-getSupport').on('click', function () {
    $('.modal-wrapper--support').fadeIn();
  });

  $('.mobile-off-canvas-menu__close').on('click', function () {
    $('#off-canvas-menu').removeClass('is-open');
    $('.js-off-canvas-overlay').removeClass('is-visible')
  });

// Change active select styles
  $('.lawyers-filter__select').on('change', function () {
    if ($(this).val() !== '') {
      $(this).addClass('active-selection')
    } else {
      $(this).removeClass('active-selection')
    }
  });

// When the user scrolls the page, execute myFunction
  $(window).scroll(function () {
    newsProgressBar()
  });

});

$(window).resize(function () {
  sctickyHeader();
});
